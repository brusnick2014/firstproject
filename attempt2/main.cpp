#include <iostream>
#include <fstream>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;
int main()
{
  Mat img = imread("/home/maxim/projects/attempt2/image.png", CV_LOAD_IMAGE_UNCHANGED);
  Mat M(Size(img.cols,img.rows),CV_8UC1);
  Mat T(Size(img.cols,img.rows),CV_32FC1);
  cvtColor( img, M, CV_BGR2GRAY );
if (img.empty())
{ cout << "Error : Image cannot be loaded..!!" << endl;
  return -1;
}
 imwrite( "/home/maxim/projects/attempt2/gray_image.png", M );

 namedWindow("MyWindow", CV_WINDOW_AUTOSIZE);
imshow("MyWindow", img);
// /////////////////////////////////////////////////
//первая часть прямое преобразование
// ////////////////////////////////////////////////
for (int i=0;i<img.rows;i++) {
    for (int j=0;j<img.cols/2;j++) {
     T.at<float>(i,j)=(M.at<int8_t>(i,j*2)+M.at<int8_t>(i,2*j+1))/2;

    }
}
for (int i=0;i<img.rows;i++) {
    for (int j=img.cols/2;j<img.cols;j++) {
     T.at<float>(i,j)=M.at<int8_t>(i,j*2-img.cols)-M.at<int8_t>(i,j*2-(img.cols-1));

    }
}
Mat P(Size(img.cols,img.rows),CV_32FC1);
for (int i=0;i<img.rows;i++) {
    for (int j=0;j<img.cols/2;j++) {
     P.at<float>(j,i)=(T.at<float>(j*2,i)+T.at<float>(j*2+1,i))/2;
                                   }
                             }
for (int i=0;i<img.rows;i++) {
    for (int j=img.cols/2;j<img.cols;j++) {
     P.at<float>(j,i)=T.at<float>(j*2-img.rows,i)-T.at<float>(j*2-(img.rows-1),i);

                                           }
                              }

// ///////////////////////////////
//обратное преобразование первая часть
// ///////////////////////////////
Mat Po(Size(img.cols,img.rows),CV_32FC1);
P.copyTo(Po);



for (int i=0;i<img.rows/2;i++) {

  for (int j=0;j<img.cols;j++) {

   Po.at<float>(i*2,j)=(P.at<float>(i,j)*2+P.at<float>(i+img.rows/2,j))/2;
}

}

for (int i=0;i<img.rows/2;i++) {

  for (int j=0;j<img.cols;j++) {

   Po.at<float>(i*2+1,j)=((P.at<float>(i,j)*2-P.at<float>(i+img.rows/2,j)))/2;
                                }

                                }
// ///////////////////////////////////
// обратное преобразование вторая часть
// ///////////////////////////////////
Mat Po2(Size(img.cols,img.rows),CV_8UC1);

for (int i=0;i<img.rows;i++) {
for (int j=0;j<img.cols/2;j++) {
 Po2.at<int8_t>(i,j*2)=((Po.at<float>(i,j))*2+Po.at<float>(i,j+img.cols/2))/2;
}
}

for (int i=0;i<img.rows;i++) {
for (int j=0;j<img.cols/2;j++) {

  Po2.at<int8_t>(i,j*2+1)=((Po.at<float>(i,j))*2-Po.at<float>(i,j+img.cols/2))/2;
}
}

imwrite( "/home/maxim/projects/attempt2/Primw1.jpeg", Po2);
imshow("Back", Po2);
imshow("Result", P);

ofstream out;

out.open("/home/maxim/projects/attempt2/POSLEO.txt");

out<<Po2;

out.close();



ofstream out1;

out1.open("/home/maxim/projects/attempt2/do.txt");

out1<<img;

out1.close();



waitKey(0);
destroyWindow("MyWindow");
return 0;
}
/*//обратное преобр ч.2

Mat Pobr(Size(img.cols,img.rows),CV_8UC1);
P1.copyTo(Pobr);

for (int i=0;i<img.rows/4;i++) {

  for (int j=0;j<img.cols/2;j++) {

   Pobr.at<int8_t>(i*2,j)=(P1.at<int8_t>(i,j)*2+P1.at<int8_t>(i+img.rows/4,j))/2;
}

}

for (int i=0;i<img.rows/4;i++) {

  for (int j=0;j<img.cols/2;j++) {

   Pobr.at<int8_t>(i*2+1,j)=((P1.at<int8_t>(i,j)*2-P1.at<int8_t>(i+(img.rows)/4,j)))/2;
}

}

// о 2

Mat Pobr2(Size(img.cols,img.rows),CV_8UC1);
Pobr.copyTo(Pobr2);

for (int j=0;j<img.rows/4;j++) {
for (int i=0;i<img.cols/2;i++) {

  Pobr2.at<int8_t>(i,j*2)=((Pobr.at<int8_t>(i,j))*2+Pobr.at<int8_t>(i,j+(img.cols/4)))/2;
}

}


for (int j=0;j<img.rows/4;j++) {
for (int i=0;i<img.cols/2;i++) {

  Pobr2.at<int8_t>(i,j*2+1)=((Pobr.at<int8_t>(i,j))*2-Pobr.at<int8_t>(i,j+(img.cols/4)))/2;
}


}
*/

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;
int main( int argc, const char** argv )
{
  Mat img = imread("/home/maxim/projects/attempt2/image.jpeg", CV_LOAD_IMAGE_UNCHANGED);
 Mat img_grey;
if (img.empty())
{
cout << "Error : Image cannot be loaded..!!" << endl;

return -1;
}
cvtColor(img,img_grey, BGR2GREY) ;
imwrite("/home/maxim/projects/attempt2/image_grey.jpeg",img_grey);

namedWindow("MyWindow", CV_WINDOW_AUTOSIZE);
imshow("MyWindow", img);

waitKey(0);

     destroyWindow("MyWindow");

return 0;
}

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <math.h>

#define N 32

using namespace std;
using namespace cv;


int main(int argc, char* argv[]){
    if(argc != 2){
        cout << "Too few arguments" << endl;
        system("pause");
        return(-1);
    }
    Mat Image(N, N, CV_32FC1, Scalar(0, 0, 0, 0));
    ifstream in(argv[1]);
    if(!(in.is_open())){
        cout << "Error of opening file" << endl;
        system("pause");
        return(-1);
    }
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            float tmp;
            in >> tmp;
            Image.at<float>(i,j) = tmp;
        }
    }
    normalize(Image, Image, 0, 1, NORM_MINMAX);
    namedWindow("SourceImage", CV_WINDOW_FREERATIO);
    imshow("SourceImage", Image);
    int Row = Image.rows, Col = Image.cols;
    Mat Zero(Row, Col, CV_32FC1, Scalar(0, 0, 0));
    Mat BufMat(Row, Col, CV_32FC1, Scalar(0, 0, 0));
    for (int k=Row; k>=1; k/=2){
        for (int i=0; i<Row; i++){
            for(int j=0; j<Col/2; j++){
                BufMat.at<float>(i,j) = (Image.at<float>(i,2*j) + Image.at<float>(i,2*j+1))/2;
                BufMat.at<float>(i, Col/2+j) = fabs((Image.at<float>(i,2*j) - Image.at<float>(i,2*j+1))/2);
            }
        }
        Image = BufMat;
        BufMat = Zero;
        for(int i=0; i<Col; i++){
            for(int j=0; j<Row/2; j++){
                BufMat.at<float>(j,i) = (Image.at<float>(2*j,i) + Image.at<float>(2*j+1,i))/2;
                BufMat.at<float>(Row/2+j, i) = fabs((Image.at<float>(2*j,i) - Image.at<float>(2*j+1,i))/2);
            }
        }
        Image = BufMat;
        Mat TMP;
        normalize(BufMat, TMP, 0, 1, NORM_MINMAX);
        namedWindow("Step", CV_WINDOW_FREERATIO);
        imshow("Step", TMP);
        cout << k << endl;
        waitKey();
    }
    ofstream Res("ResOfExpansion.dat");
    for(int i=0; i<Row; i++){
        for(int j=0; j<Col; j++){
            Res << Image.at<float>(i,j) << " ";
        }
        Res << endl;
    }
    Res.close();
    in.close();
    return(0);
}
